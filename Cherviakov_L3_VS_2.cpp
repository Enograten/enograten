#include <time.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
#include <math.h>
extern int p_m(int *f, int nf, int mf);
extern double se(int *f, int nf, int mf); 
using namespace std;

int main()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(NULL));
	int n, m;
	cout << "n = ";
	cin >> n;
	cout << "m = ";
	cin >> m;
	int a[n][m];
	cout << "\nMatr["<<n<<"]["<<m<<"]\n";
	p_m(&a[0][0], n, m);
	cout << "\n\nPeretvor matr["<<n<<"]["<<m<<"]\n";
	se(&a[0][0], n, m);
	cout << "\n";
	system("pause");
	return 0;
}
using namespace std;

int p_m(int *f, int nf, int mf)
{
	int i, j;
	for(i = 0; i < nf; i++)
	{
		for(j = 0; j < mf; j++)
		{
			*(f + i*mf + j)=rand()%100-50;
			printf("%3d\t",*(f + i*mf + j));	
		}
	cout << endl;
	}
	return 0;
}

double se(int *f, int nf, int mf)
{
	int i, j;
	for(i = 0; i < nf; i++)
	{
		for(j = 0; j < mf; j++)
		{
			if(*(f + i*mf + j)>=0)
			*(f + i*mf + j)=0;
			else
			*(f + i*mf + j)=1;
			printf("%1d  ",*(f + i*mf + j));
		}
	cout << endl;
	}
	return 0;
}
